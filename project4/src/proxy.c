#include <stdio.h>
#include <stdlib.h>
#include "thread.h"
#include "proxy.h"

int main(int argc, char *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage: %s proxyHostName proxyPort\n", argv[0]);
		exit(1);
	}
	
	int errors[3];
	errors[0] = atoi(argv[3]);
	errors[1] = atoi(argv[4]);
	errors[2] = atoi(argv[5]);

	// Setup connections
	struct sockaddr_in *destAddr = connectToAddr(argv[1], atoi(argv[2]));
	struct sockaddr_in *sendAddr = listenAddr();
	int sock = bindSocket(sendAddr);

	// Print out our port so sender can connect
	printf("Listening on port %i\n", getPort(sock, sendAddr));

	listenForPackets(sock, destAddr, errors);
	
	return 0;
}

void listenForPackets(int sock, struct sockaddr_in *receiverAddr, int errors[]) {
	char buf[BUFFER_SIZE];
	int gotSenderAddr = 0;
	struct sockaddr_in senderAddr;
	struct sockaddr_in fromAddr;
	socklen_t size = sizeof(fromAddr);
	
	struct packet packet;
	
	while (1) {
		memset(buf, 0, BUFFER_SIZE);
		if (recvfrom(sock, buf, BUFFER_SIZE, 0, (struct sockaddr *) &fromAddr, &size) < 0)
			fatalError("Error while receiving data");

		// Initialize our packet data and attach the socket
		initPacket(&packet, &buf[0], sock);

		// Pass all the address into setDestAddr so it can figure out where this packet came from
		// and where it's going next
		setDestAddr(&packet, &fromAddr, receiverAddr, &senderAddr);

		// The first time the sender connects, we need to remember the connection so we can forward
		// the receiver's ACK back to the sender
		if (gotSenderAddr == 0 && packet.fromReceiver == 0) {
			memcpy(&senderAddr, &fromAddr, sizeof(fromAddr));
			gotSenderAddr = 1;
		}
		
		if (packet.fromReceiver == 0 && packet.data[2] == '1') {
			gotSenderAddr = 0;
		}
		
		forwardPacket(&packet, errors);
	}
}

void initPacket(Packet packet, char *data, int sock) {
	packet->data = data;
	packet->sock = sock;
}

void setDestAddr(Packet packet, struct sockaddr_in *fromAddr, struct sockaddr_in *receiverAddr,
                 struct sockaddr_in *senderAddr) {
	char fromIPaddr[BUFFER_SIZE], receiverIPaddr[BUFFER_SIZE];
	int fromPort, receiverPort;

	// Get IP address and port of receiver
	inet_ntop(AF_INET, (void *)(&receiverAddr->sin_addr), receiverIPaddr, (socklen_t) BUFFER_SIZE);
	receiverPort = ntohs(receiverAddr->sin_port);
	
	// Get IP address and port of sender
	inet_ntop(AF_INET, (void *)(&fromAddr->sin_addr), fromIPaddr, (socklen_t) BUFFER_SIZE);
	fromPort = ntohs(fromAddr->sin_port);

	// Compare receiver address to the unknown sender of the packet
	if (strcmp(receiverIPaddr, fromIPaddr) == 0 && fromPort == receiverPort) {
		packet->fromReceiver = 1;
		packet->destAddr = senderAddr;
	}
	else {
		packet->destAddr = receiverAddr;
		packet->fromReceiver = 0;
	}
}

Packet copyPacket(Packet packet) {
	Packet newPacket = malloc(sizeof(Packet*));
	newPacket->data = malloc(sizeof(char) * strlen(packet->data));
	
	strcpy(newPacket->data, packet->data); 
	newPacket->destAddr = *(&packet->destAddr);
	newPacket->sock = packet->sock;
	newPacket->fromReceiver = packet->fromReceiver;
	
	return newPacket;
}

void freePacket(Packet packet) {
	free(packet->data);
	free(packet);
}

void forwardPacket(Packet packet, int errChance[]) {
	int lost = randomInt(100), 
			error = randomInt(100), 
			delayed = randomInt(100);

	if (lost > errChance[0]) {
		if (error < errChance[2]) {
			// Set corrupt bit
			packet->data[0] = '1';
		}
		if (delayed < errChance[1]) {
			// Delay packet by creating new thread then send packet
			createThread(delayPacket, (void *) copyPacket(packet));
		}
		else {
			// Wait a little while to simulate latency
			usleep(PACKET_LATENCY_LENGTH);
			
			// Send packet to its destination
			sendPacket(packet);
		}
	}
}

void *delayPacket(void *data) {
	Packet packet = (Packet) data;
	
	usleep(PACKET_DELAY_LENGTH);

	sendPacket(packet);
	freePacket(packet);
	
	return NULL;
}

/*
 * Sends a packet to its destination
 */
void sendPacket(Packet packet) {
	socklen_t destLen = sizeof(*packet->destAddr);
	if (sendto(packet->sock, packet->data, 10, 0, (struct sockaddr *) packet->destAddr, destLen) < 0) 
		fatalError("Couldn't send packet");
}

/**
* Pulls entropy from /dev/urandom
* Generates a 32-bit random int
* Comparable to the IPv4 address space
*/
int randomInt(int max) {
	FILE *prng;
	unsigned int val;

	if ((prng = fopen("/dev/urandom", "r")) == NULL)
		fatalError("Error accessing PRNG");

	fread(&val, 1, sizeof(int), prng); // Grab some random data
	fclose(prng);

	return val % max;
}