#include "common.h"

#define PACKET_DELAY_LENGTH 75000
#define PACKET_LATENCY_LENGTH 20000

typedef struct packet {
	char *data;
	int sock;
	int fromReceiver;
	struct sockaddr_in *destAddr;
} *Packet;

void listenForPackets(int, struct sockaddr_in*, int[]);
void forwardPacket(Packet, int []);
void freePacket(Packet);
void *delayPacket(void *);
void sendPacket(Packet);
void initPacket(Packet, char *, int);
void setDestAddr(Packet, struct sockaddr_in*, struct sockaddr_in*, struct sockaddr_in*);
int randomInt(int);