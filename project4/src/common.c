#include "common.h"

/**
* Parses the given buffer and assigns the packet type and data to the packet struct
*/
Comm_Packet parseCommPacket(Comm_Packet packet, char *buffer, struct sockaddr_in peerAddr) {
	// Comm packet format
	// <type>:<data>
	
	packet->type = atoi(&buffer[0]); // The type is the first character
	packet->data = &buffer[2]; // The data starts at the third character, after the colon
	
	socklen_t len = BUFFER_SIZE;
	inet_ntop(AF_INET, &(peerAddr.sin_addr), packet->IPaddr, len); // Save IP address of client
	packet->port = ntohs(peerAddr.sin_port); // Save port of client
	packet->from = peerAddr;

	return packet;
}

/**
* Sends a packet to a peer
*/
int sendToPeer(Peer peer, int sock, int type, char *data) {
	return sendCommPacket(type, sock, peer->sockAddr, data);
}

/**
* Low-level function to send packet to an address
*/
int sendCommPacket(int type, int sockFD, struct sockaddr_in addr, char *data) {
	char *packetData = buildPacketData(type, data);
	socklen_t addrLen = sizeof(addr);

	int result = sendto(sockFD, packetData, strlen(packetData) + 1, 0, (struct sockaddr *) &addr, addrLen);
	
	free(packetData);
	return result;
};

/**
* Creates a buffer for the given packet type and data
*/
char *buildPacketData(int type, char *data) {
	int len = strlen(data) + 1; // Length of our data
	int packetSize = 2 + len; // 2 chars for the packet type and colon

	// Allocate buffer to hold IP address
	char *buffer = malloc(sizeof(char) * packetSize);
	memset(buffer, 0, packetSize);

	// Write packet data to buffer
	snprintf(buffer, packetSize, "%i:%s", type, data);

	return buffer;
}

/**
* Creates a new peer from the packet data
*/
Peer newPeerFromPacket(Comm_Packet packet) {
	return newPeer(packet->IPaddr, packet->port, packet->from);
}

/**
* Allocates memory for a new peer from the given IP address, port and address struct
*/
Peer newPeer(char *IPaddr, int port, struct sockaddr_in peerAddr) {
	Peer peer = malloc(sizeof(struct peer));
	
	// Copy data from packet
	peer->IPaddr = strdup(IPaddr);
	peer->port = port;
	peer->sockAddr = peerAddr;

	return peer;
}

/**
* Frees allocated peer memory
*/
void freePeer(Peer peer) {
	free(peer->IPaddr);
	free(peer);
}

/**
* Used to create an address struct from an IP address and port
*/
struct sockaddr_in *connectToAddr(char *IPaddr, int port) {
	struct sockaddr_in *addr = malloc(sizeof(struct sockaddr_in));

	// Set up server address
	memset(addr, 0, sizeof(*addr));
	addr->sin_family = AF_INET;
	inet_pton(AF_INET, IPaddr, &(addr->sin_addr));
	addr->sin_port = htons(port);
	
	return addr;
}

/**
* Used to create an address struct meant for listening
*/
struct sockaddr_in *listenAddr() {
	struct sockaddr_in *addr = malloc(sizeof(struct sockaddr_in));

	// Set up server address
	memset(addr, 0, sizeof(*addr));
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = htonl(INADDR_ANY);
	addr->sin_port = htons(0);
	
	return addr;
}

/**
* Helper function to print error messages
*/
void fatalError(char *err) {
	perror(err);
	exit(1);
}

/**
* Clears the buffer of unwanted newline chars
*/
void clearBuffer() {
	while (getchar() != '\n');
}

int bindSocket(struct sockaddr_in *sendAddr) {
	// Open a new socket
	socklen_t sendLen = sizeof(*sendAddr);

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		fatalError("Error opening socket");

	// Bind to address
	if (bind(sock, (struct sockaddr *) sendAddr, sendLen) < 0)
		fatalError("Error binding to address");

	return sock;
}

int getPort(int sock, struct sockaddr_in *sendAddr) {
	socklen_t size = sizeof(struct sockaddr);
	getsockname(sock, (struct sockaddr *) sendAddr, &size);
	return ntohs(sendAddr->sin_port);
}