#include "common.h"
#include "rdtSender.h"


int main(int argc, char *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage: %s proxyHostName proxyPort\n", argv[0]);
		exit(1);
	}

	char msg[BUFFER_SIZE];

	while (1) {
		puts("Enter a message:");
		// Clear the buffer before getting user message
		fgets(msg, BUFFER_SIZE, stdin);
		if (!sendMessage(argv[1], atoi(argv[2]), (char *)&msg))
			fatalError("Couldn't send message to receiver!");
		else
			printf("Message sent!\n");
	}

	return 0;
}


int sendMessage(char *desthost, int destPort, char *message) {
	struct sockaddr_in *destAddr = connectToAddr(desthost, destPort);
	struct sockaddr_in *sendAddr = listenAddr();
	int sock = bindSocket(sendAddr);

	return sendPackets(message, sock, sendAddr, destAddr);
}


int sendPackets(char *message, int sock, struct sockaddr_in *sendAddr, struct sockaddr_in *destAddr) {
	struct sockaddr_in fromAddr;
	socklen_t destLen = sizeof(*destAddr);
	socklen_t fromLen = sizeof(fromAddr);
	int seqNum = 0, offset = 0;
	char sendBuf[BUFFER_SIZE], receiveBuf[BUFFER_SIZE];
	struct timeval tv;
	
	fd_set readfds;
	
	
	
	while (offset < strlen(message)) {
		memset(sendBuf, 0, BUFFER_SIZE);
		snprintf(sendBuf, BUFFER_SIZE, "0%i%i", seqNum, (offset + DATA_LENGTH >= strlen(message)));
		strncpy(&sendBuf[3], &(message[offset]), DATA_LENGTH);
		
		int gotAck = 0, rv = 0;
		
		while (gotAck == 0) {
			tv.tv_sec = TIMEOUT_LENGTH_SEC;
			tv.tv_usec = TIMEOUT_LENGTH_USEC;
			
			// Only send again if we haven't sent this packet
			// or if the timeout elapsed
			if (!rv && sendto(sock, sendBuf, 10, 0, (struct sockaddr *) destAddr, destLen) < 0) 
				fatalError("Couldn't send packet");

			FD_ZERO(&readfds);
			FD_SET(sock, &readfds);
			
			rv = select(sock + 1, &readfds, NULL, NULL, &tv);

			if (FD_ISSET(sock, &readfds)) {
				// Check seq num
				if (recvfrom(sock, receiveBuf, BUFFER_SIZE, 0, (struct sockaddr *) &fromAddr, &fromLen) < 0)
					fatalError("Error while receiving data");

				int corrupt = (receiveBuf[0] == '1');
				int ackNum = (receiveBuf[1] == '1');
				
				if (!corrupt && ackNum == seqNum) {
					gotAck = 1;
				}
			}
		}
		
		seqNum = (seqNum + 1) % 2;
		
		offset += DATA_LENGTH;
		if (offset > strlen(message))
			offset = strlen(message);
	}
	
	return 1;
}
