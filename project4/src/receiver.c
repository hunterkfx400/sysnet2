#include "rdtReceiver.h"
#include "common.h"


int main(int argc, char *argv[]) {
	char *msg;
	
	if (argc < 2) {
		fprintf(stderr, "Usage: %s port\n", argv[0]);
		exit(1);
	}

	printf("Waiting on port %i...\n", atoi(argv[1]));
	
	while (1) {
		msg = receiveMessage(atoi(argv[1]));
		printf("Got message: %s", msg);
	}
	
	return 0;
}


char *receiveMessage(int port) {
	int sock;
	struct sockaddr_in addr;
	
	// Open a new socket
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		fatalError("Error opening socket");

	// Set up receiver address
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);

	
	// Need this to allow resuse of port
	int yes = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		fatalError("Setsockopt");
	}

	// Bind to address
	if (bind(sock, (struct sockaddr *) &addr, sizeof addr) < 0) {
		if (errno != EADDRINUSE)
			fatalError("Error binding to address");
	}

	char buf[BUFFER_SIZE];
	char *message = malloc(sizeof(char) * BUFFER_SIZE);
	char ack[BUFFER_SIZE];
	int ackNum, expectedAck = 0;
	struct sockaddr_in peerAddr;
	socklen_t size = sizeof(peerAddr);
	int end = 0;
	
	while (end != 1) {
		memset(buf, 0, BUFFER_SIZE);
		if (recvfrom(sock, buf, BUFFER_SIZE, 0, (struct sockaddr *) &peerAddr, &size) < 0)
			fatalError("Error while receiving data");

		if (buf[0] == '0') {
			ackNum = (buf[1] == '1');
			end = (buf[2] == '1');
			
			//sendAck
			if (ackNum == expectedAck) {
				strcat(message, &buf[3]);
				expectedAck = (expectedAck + 1) % 2;
			}

			snprintf(ack, BUFFER_SIZE, "0%i1ACK", ackNum);
			sendto(sock, ack, strlen(ack), 0, (struct sockaddr *) &peerAddr, size);
		}
	}
	
	// Got all packets, return message
	
	return message;
}