# BB Network Protocol Doc
> This document is best viewed when interpreted as [Markdown](https://help.github.com/articles/markdown-basics/)

## Packet Format
All BB Network communication packets conform to the following format:
> packetType:packetData

## Packet Types
- Announce
	- Sent from peer to server when a peer wishes to join the BB network
	- No data is required to be sent
- Waiting for Peers
	- Sent from server to all peers when a new peer has joined the BB network
	- Data contains the number of remaining peers
- Peer Address
	- Sent from server to each peer when all peers have connected
	- Data contains the IP address and port of another peer
- Leader Election
	- Sent by all peers to each other in order to determine initial token creation
	- Data contains the vote of another peer
- Token
	- Sent by all peers to each other to indicate who currently has the token
	- Data may optionally contain info of peer who wishes to leave the BB Network


## BB Network Phases
- Bootstrapping phase
- Leader Election phase
- Token Ring phase

## Server / Peer Communication Overview
In the bootstrapping phase of the BB Network, the server and peers communicate with one another. At this stage, the peers are not yet talking to each other - only to the server.

### Announce
Once the server has been started, it will wait for the chosen number of peers to connect. To announce its desire to join the BB Network, a peer will send an **Announce packet** to the server. When an **Announce packet** is received by the server, it will add that peer to a list of peers who are awaiting the next phase of the BB Network.

### Waiting for Peers
Each time an **Announce packet** is received by the server, it will send a **Waiting for Peers packet** to the list of peers, letting them know how many more peers need to connect before the next phase can begin. The data sent in the **Waiting for Peers packet** contains the **number of remaining peers** that must connect before the next phase begins.

### Peer Address
Once the required number of peers have connected to the server, the server will send a **Peer Address packet** to each peer. The packet data contains the **IP address** and the **port** of another peer to which the receiving peer should connect to in order to form the ring topology.

This concludes the bootstrapping phase of the BB Network. The server exits after it finishes sending out all **Peer Address packets**.

## Peer / Peer Communication Overview
In the Leader Election phase of the BB Network, each peer has connected to another peer in a ring topology. After making the connection to its neighbor, a peer will generate a random 32-bit integer, or **vote**, which it will use to determine who will be the network leader. The peer will send a **Leader Election packet** which contains its **vote** to its neighbor.The sole purpose of the network leader is to generate the token.

### Vote Protocol
When a peer receives the **Leader Election packet** from another peer, it will check that peer's **vote** against its own. If the **received vote** is *greater* than the **local vote**, then the peer can be assured that it is *not* the leader. The peer then forwards the **received vote** onto the next peer.

If the **received vote** is *less* than the **local vote**, then the peer is still a candidate for network leader and awaits further **Leader Election packets**. 

If the **received vote** is *equal* to the **local vote**, then the peer knows that the **local vote** has made it all the way around the ring and back to the peer. At this point, the peer assumes the role of network leader. The network leader then sends the **Token packet** to its neighbor to start the final phase of the BB Network.


## Token Ring Overview
In the Token Ring phase of the BB Network, the token has been generated as is being passed around the network by the peers. The peers will continue to pass the token around until a peer decides to exit the network. 

To indicate a desire to leave the BB Network, a peer will wait until it receives the **Token packet**. Once the packet is received, the peer will attach data to the packet before forwarding it to its neighbor. The peer will attach three pieces of data: its own port, the IP address of its neighbor and the port of its neighbor. These three pieces of data are enough to ensure the ring remains stable after the peer leaves the network.

### Token Actions
#### Forward 
When a peer receives a **Token packet** which has data attached, it will parse the data to determine if any action needs to be taken. The peer will first look at the leaving peer's port and compare it with both its own port and the port of its neighbor. If the leaving peer's port matches *neither* of these, the peer merely *forwards* the packet along with the original data.

#### Reconnect
However, if the leaving peer's port *matches* its neighbor's port, then it must take action to prevent the collapse of the ring topology. The peer will extract the IP address and port of the leaving peer's neighbor from the **Token packet**. Then it will *forward* the **Token packet** along with the original data to its neighbor. *After* forwarding the packet, the peer will disconnect from its neighbor, and reconnect to the IP address and port previously extracted from the packet, thus ensuring the continued operation of the token ring.

#### Exit
When a peer announces its desire to leave the network, it must wait to ensure all other peers have seen the announcement. Once the peer receives the **Token packet** again, containing the data the peer originally sent, it can be assured that all other peers have seen it and have taken the necessary actions. At this point, the peer is no longer part of the ring topology and is free to exit at any time.