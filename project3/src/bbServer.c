
#include "bbServer.h"


struct sockaddr_in serverAddr;
int totalPeers, serverSock, connectedPeers = 0; // Keep track of peer connection status
Peer *peers;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		fprintf(stderr, "usage: bbServer <numHosts>\n");
		exit(1);
	}
	
	totalPeers = atoi(argv[1]);
	peers = malloc(sizeof(Peer *) * totalPeers);
	
	// Open a new socket
	serverSock = socket(AF_INET, SOCK_DGRAM, 0);
	if (serverSock < 0)
		fatalError("Error opening socket");
	
	// Set up server address
	memset(&serverAddr, 0, sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(0);
	
	// Bind to address
	if (bind(serverSock, (struct sockaddr *) &serverAddr, sizeof serverAddr) < 0)
		fatalError("Error binding to address");

	// Print out our port so clients can connect
	socklen_t size = sizeof(struct sockaddr);
	getsockname(serverSock, (struct sockaddr *) &serverAddr, &size);
	printf("Waiting on port %i for %i peers to connect...\n", ntohs(serverAddr.sin_port), totalPeers);


	char buf[BUFFER_SIZE];
	struct comm_packet packet;
	struct sockaddr_in clientAddr;
	int remainingPeers;
	
	// Loop until all peers have connected
	while (connectedPeers < totalPeers) {
		memset(buf, 0, BUFFER_SIZE);
		if (recvfrom(serverSock, buf, BUFFER_SIZE, 0, (struct sockaddr *) &clientAddr, &size) < 0)
			fatalError("Error while receiving data");

		// Got a new packet, parse it
		parseCommPacket(&packet, buf, clientAddr);
		
		// Is it an announce packet?
		if (packet.type == PACKET_TYPE_ANNOUNCE) {
			remainingPeers = handleAnnounce(&packet);

			// Are we still waiting on peers?
			if (remainingPeers > 0) {
				// Tell peers that a new peer has connected
				sendWaitingPeers();
				printf("Waiting for %i more peers to connect...\n", remainingPeers);
			}
		}
	}

	// Tell peers who their neighbors are
	// Begins the next phase of the BB Network
	sendPeerNeighbors();

	return 0;
}

/**
* A new peer has connected. Saves the peer connection data and returns how many peers we are still waiting for.
*/
int handleAnnounce(Comm_Packet packet) {
	Peer peer = newPeerFromPacket(packet);
	peers[connectedPeers++] = peer;

	return totalPeers - connectedPeers;
}

/**
* Sends the number of peers we are waiting on to each peer
*/
void sendWaitingPeers() {
	char buf[10];
	memset(buf, 0, 10);
	snprintf(buf, 10, "%i", totalPeers - connectedPeers);
	int x;
	for (x = 0; x < connectedPeers; x++) {
		sendToPeer(peers[x], serverSock, PACKET_TYPE_WAITING_PEERS, buf);
	}
}

/**
* Send all peers their neighbor's connection data
*/
void sendPeerNeighbors() {
	int x;
	char neighborData[BUFFER_SIZE];
	Peer peer;
	for (x = 0; x < totalPeers; x++) {
		// Get peer's neighbor
		peer = getNeighbor(x);
		// Turn neighbor data into a string
		serializeNeighbor((char *) &neighborData, peer);
		// Send the neighbor data
		sendToPeer(peers[x], serverSock, PACKET_TYPE_NEIGHBOR_ADDR, neighborData);
	}
}

/**
* Determines who is the neighbor of the given peer
*/
Peer getNeighbor(int peer) {
	int neighbor = (peer + 1) % connectedPeers;
	return peers[neighbor];
}

/**
* Writes neighbor data to the given buffer
*/
void serializeNeighbor(char *buffer, Peer neighbor) {
	memset(buffer, 0, BUFFER_SIZE);
	snprintf(buffer, BUFFER_SIZE, "%s,%i", neighbor->IPaddr, neighbor->port);
}
