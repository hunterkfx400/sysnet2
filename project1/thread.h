/**
* @file thread.c
* @brief Prototypes for thread wrapper
*
* @author Ben Avellone
* @author Hunter Hardy
*
*/

#ifndef MY_THREADS
#define MY_THREADS 1
#include <pthread.h>

pthread_t createThread(ThreadPool, void *, void **);
void joinThread(pthread_t);

#endif
