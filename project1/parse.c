#include "TCPserver.h"

void parse(char *input, char **tokens) {
	char *str = malloc(sizeof(char) * messageSize);
	int num = 0, x;

	for (x = 0; x < 5; x++)
		tokens[x] = malloc(sizeof(char) * messageSize);

	// Tokenize input
	str = strtok(input, "<>");
	while (str != NULL) {
		tokens[num++] = str;
		str = strtok(NULL, "<>");
	}
}

void process(char **tokens, int socket) {
	char echoTagFront[] = "echo";
	char echoTagRear[] = "/echo";
	char loadavgTagFront[] = "loadavg";
	char loadavgTagRear[] = "loadavg/";
	char errorMessage[] = "<error>unknown format</error>";
	char src[messageSize], dest[messageSize];

	if (strcmp(echoTagFront, tokens[0]) != 0 && strcmp(echoTagRear, tokens[2]) != 0) {
		strcpy(src, "reply");
		strcpy(dest, tokens[1]);
		strcat(src, dest);
		strcpy(dest, "/reply");
		strcat(src, dest);
		write(socket, src, strlen(src));
	}
	else if (strcmp(loadavgTagFront, tokens[0]) != 0 && strcmp(loadavgTagRear, tokens[2]) != 0) {
		double averages[3];

		getloadavg(averages, 3);

		strcpy(src, "replyLoadAvg");
		strcpy(dest, tokens[1]);
		strcat(src, dest);
		strcpy(dest, "/replyLoadAvg");
		strcat(src, dest);
		write(socket, src, strlen(src));
	}
	else {
		write(socket, errorMessage, strlen(src));
	}
}