/*
    gcc server.c -lpthread -o server
*/

#include "TCPserver.h"


//the thread function
void *connection_handler(void *);

int main(int argc, char *argv[]) {
	int serversocket, client_sock, c;
	socklen_t size = sizeof(struct sockaddr);
	struct sockaddr_in server, client, host;

	//Create socket
	serversocket = socket(AF_INET, SOCK_STREAM, 0);

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(8888);

	//Bind
	bind(serversocket, (struct sockaddr *) &server, sizeof(server));

	//Listen
	listen(serversocket, 1024);

	getsockname(serversocket, (struct sockaddr *) &host, &size);

	//Accept and incoming connection
	fprintf(stderr, "Waiting for incoming connections on: %s:%i", inet_ntoa(server.sin_addr), server.sin_port);
	c = sizeof(struct sockaddr_in);
	pthread_t thread_id;

	while ((client_sock = accept(serversocket, (struct sockaddr *) &client, (socklen_t *) & c))) {
		puts("Connection accepted");
		pthread_create(&thread_id, NULL, connection_handler, (void *) &client_sock);
		pthread_join(thread_id, NULL);
		puts("Handler assigned");
	}
	return 0;
}


/*
 * This will handle connection for each client
 * */
void *connection_handler(void *socket_desc) {
	//Get the socket descriptor
	int sock = *(int *) socket_desc;
	char tokens[3][messageSize];
	char client_message[messageSize];

	//Receive a message from client
	recv(sock, client_message, messageSize, 0);
	parse(client_message, (char **)tokens);
	process((char **)tokens, sock);

	//free tokens

	return 0;
} 